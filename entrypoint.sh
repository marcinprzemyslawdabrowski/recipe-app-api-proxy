#!/bin/sh

set -e  #return failure even one line is wrong

# pass env variable to swap {} -> to new file
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf
nginx -g 'daemon off;'  #nginx runs in background, but it should run in foreground